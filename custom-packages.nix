{ system ? builtins.currentSystem }:
let
  pkgs = import <nixpkgs> { inherit system; };

  callPackage = pkgs.lib.callPackageWith ( pkgs // pkgs.xlibs // self );

  self = rec {

    inherit callPackage; # needed so it finds deps inside here ...

    srtp-dl = callPackage ./pkgs/development/libraries/srtp-dl {  };

    #doubango-generic = pkgs.lib.makeOverridable (callPackage ./pkgs/development/libraries/doubango/generic.nix) {};
    doubango-generic-args = {
      srtpSupport = true;
      sslSupport = true;
      ffmpegSupport = true;
      vpxSupport = true;
      opusSupport = true;
      speexSupport = true;
      speexdspSupport = true;
    };
    # need to send empty doubangoConfig so .override can be used ... TODO why ?
    doubango-generic = callPackage ./pkgs/development/libraries/doubango/generic.nix ( doubango-generic-args // { doubangoConfig = {}; } );
    doubango-generic-with = args: callPackage ./pkgs/development/libraries/doubango/generic.nix ( doubango-generic-args // args );
    doubango-gcdb-svn-branch-2x0-r1326 = callPackage ./pkgs/development/libraries/doubango/svn.nix ( doubango-generic-args // {
      doubangoConfigSvn = {
        # ~/.nix-defexpr/channels/nixpkgs/pkgs/build-support/fetchsvn/nix-prefetch-svn https://doubango.googlecode.com/svn/branches/2.0 1328
        type = "branch";
        name = "gcdb";
        version = "2.0";
        revision = "1328";
        sha256 = "0q2q01saqr2g1d7kgvlqysrkw7iyx4lb623i9kxyp70wnqj62r21";
        sourceRootSuffix = "/doubango";
        patches = [ ./pkgs/development/libraries/doubango/doubango-2.0.branch-fixes.patch ]; # TODO reduce length
      };
    } );
    doubango-svn = doubango-gcdb-svn-branch-2x0-r1326;

    doubango-gldbia-git-branch-2x0-h87b8aa97 = callPackage ./pkgs/development/libraries/doubango/git.nix ( doubango-generic-args // {
      doubangoConfigGit = {
        # ~/.nix-defexpr/channels/nixpkgs/pkgs/build-support/fetchgit/nix-prefetch-git --no-deepClone --url https://gitlab.com/doubango-ia/doubango.git --rev 87b8aa9756beb9e45fbb3881f5a67c06f4f18f30
        type = "branch";
        name = "gldbia";
        version = "2.0.9999";
        revision = "87b8aa9756beb9e45fbb3881f5a67c06f4f18f30";
        sha256 = "cf3f559839ab3034ac78c1b4bcea9b32e54e60aa11d7010ef74df05c971babc6";
        sourceRootSuffix = "/doubango";
        patches = [ ./pkgs/development/libraries/doubango/doubango-2.0.branch-fixes.patch ]; # TODO reduce length
      };
    } );
    doubango-git = doubango-gldbia-git-branch-2x0-h87b8aa97;
    doubango = doubango-git;


    webrtc2sip-generic-args = {};
    webrtc2sip-generic = callPackage ./pkgs/development/libraries/webrtc2sip/generic.nix ( webrtc2sip-generic-args // { webrtc2sipConfig = {}; } );
    webrtc2sip-generic-with = args : callPackage ./pkgs/development/libraries/webrtc2sip/generic.nix ( webrtc2sip-generic-args // args);

    webrtc2sip-gcws-svn-trunk-r141 = callPackage ./pkgs/development/libraries/webrtc2sip/svn.nix ( webrtc2sip-generic-args // {
      webrtc2sipConfigSvn = {
        type = "trunk";
        name = "gcws";
        revision = "141";
        sha256 = "0jcxszw7g7sc4lxccwmrp3q0sywfkwnijp2mb9hs5hjmyqxb596p";
      };
    } );
    webrtc2sip-svn = webrtc2sip-gcws-svn-trunk-r141;

    webrtc2sip-gldbia-git-master-h61ba5c91 = callPackage ./pkgs/development/libraries/webrtc2sip/git.nix ( webrtc2sip-generic-args // {
      webrtc2sipConfigGit = {
        # ~/.nix-defexpr/channels/nixpkgs/pkgs/build-support/fetchgit/nix-prefetch-git --no-deepClone --url https://gitlab.com/doubango-ia/webrtc2sip.git --rev 61ba5c91aa21915a1e6c93d86585dbb999a151a1
        type = "trunk";
        name = "gldbia";
        version = "9999.0.0";
        revision = "61ba5c91aa21915a1e6c93d86585dbb999a151a1";
        sha256 = "379e2e4bd3cefef45ac09886a39352d0e9ab948906cf1e2a2360e615954a2322";
      };
    } );
    webrtc2sip-git = webrtc2sip-gldbia-git-master-h61ba5c91;

    webrtc2sip = webrtc2sip-git;
  };
in self
