{ srtpSupport ? false
, sslSupport ? false
, ffmpegSupport ? false
#, x264Support ? false # IMPORTANT: deprecated to be removed
#, openh264Support ? false
, vpxSupport ? false
#, yuvSupport ? false
#, jpegSupport ? false
#, amrSupport ? false
, opusSupport ? false
, speexSupport ? false
, speexdspSupport ? false
#, gsmSupport ? false
#, g729Support ? false
#, ilbcSupport ? false
#, webrtcSupport ? false
, stdenv, pkgconfig, autoconf, automake, libtool
, srtp-dl ? null
, openssl ? null
, ffmpeg_1 ? null
, libvpx ? null
, libopus ? null
, speex ? null
, speexdsp ? null
, doubangoConfig }:

assert srtpSupport -> srtp-dl != null;
assert sslSupport -> openssl != null;
assert ffmpegSupport -> ffmpeg_1 != null;
assert vpxSupport -> libvpx != null;
assert opusSupport -> libopus != null;
assert speexSupport -> speex != null;
assert speexdspSupport -> speexdsp != null;

with stdenv.lib;

stdenv.mkDerivation rec {

  #_debug_000 = builtins.trace "doubango/generic.nix: ${showVal args}" null;
  #_debug_001 = builtins.trace "doubango/generic.nix: ${showVal doubangoConfig._debug}" null;

  name = "doubango${doubangoConfig.suffix}";

  src = doubangoConfig.src;
  patches = doubangoConfig.patches or [];
  sourceRoot = doubangoConfig.sourceRoot or "";
  setSourceRoot = doubangoConfig.setSourceRoot or "";
  sourceRootSuffix = doubangoConfig.sourceRootSuffix or "";
  postUnpack = "sourceRoot=\${sourceRoot}${sourceRootSuffix}";

  enableParallelBuilding = true;

  buildInputs = [ pkgconfig autoconf automake libtool ]
    ++ stdenv.lib.optional srtpSupport srtp-dl
    ++ stdenv.lib.optional sslSupport openssl
    ++ stdenv.lib.optional ffmpegSupport ffmpeg_1
    ++ stdenv.lib.optional vpxSupport libvpx
    ++ stdenv.lib.optional opusSupport libopus
    ++ stdenv.lib.optional speexSupport speex
    ++ stdenv.lib.optional speexdspSupport speexdsp
    ;

  preConfigure = ''
    ./autogen.sh
  '';


  configureFlags = ''
    ${if srtpSupport then "--with-srtp=${srtp-dl}" else "--without-srtp"}
    ${if sslSupport then "--with-ssl=${openssl}" else "--without-ssl"}
    ${if ffmpegSupport then "--with-ffmpeg=${ffmpeg_1}" else "--without-ffmpeg"}
    --without-x264
    --without-openh264
    ${if vpxSupport then "--with-vpx=${libvpx}" else "--without-vpx"}
    --without-yuv
    --without-jpeg
    --without-amr
    ${if opusSupport then "--with-opus=${libopus}" else "--without-opus"}
    ${if speexSupport then "--with-speex=${speex}" else "--without-speex"}
    ${if speexdspSupport then "--with-speexdsp=${speexdsp}" else "--without-speexdsp"}
    --without-gsm
    --without-g729
    --without-ilbc
    --without-webrtc
    cross_compiling=yes
  '';

  meta = {
    description = "3GPP IMS/LTE framework";
    longDescription = ''
      doubango is an experimental, open source, 3GPP IMS/LTE framework for both embedded and desktop systems.
      The framework is written in ANSI-C to ease portability and has been carefully designed to efficiently work on embedded systems with limited memory and low computing power and to be extremely portable.
    '';
    homepage = https://code.google.com/p/doubango/;
    license = stdenv.lib.licenses.gpl3Plus;
    maintainers = [ "Iwan Aucamp <aucampia@gmail.com>" ];
    platforms = stdenv.lib.platforms.all;
  };
}
