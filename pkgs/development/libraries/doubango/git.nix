{ callPackage, doubangoConfigGit, fetchgit, stdenv, ... } @ args:

let
  doubangoConfigGitUse = { url = "https://gitlab.com/doubango-ia/doubango.git"; } // doubangoConfigGit;
in
let
  doubangoConfigGit = doubangoConfigGitUse;
in
callPackage ./generic.nix ( ( stdenv.lib.filterAttrs ( n: v: n != "callPackage" && n != "doubangoConfigGit" && n != "fetchgit" ) args ) // {
  doubangoConfig = {
      _debug = builtins.trace "doubango/git.nix: ${stdenv.lib.showVal args}" null;
      src = fetchgit {
        url = doubangoConfigGit.url;
        rev = doubangoConfigGit.revision;
        sha256 = doubangoConfigGit.sha256;
      };
     suffix = "-${doubangoConfigGit.version}-h${builtins.substring 0 7 doubangoConfigGit.revision}-${doubangoConfigGit.name}-git-${doubangoConfigGit.type}";
    } // doubangoConfigGit;
} )
