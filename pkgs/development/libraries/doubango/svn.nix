{ callPackage, doubangoConfigSvn, fetchsvn, stdenv, ... } @ args:

let
  doubangoConfigSvnUse = { url = "https://doubango.googlecode.com/svn"; urlSuffix = ""; } // doubangoConfigSvn;
in
let
  doubangoConfigSvn = doubangoConfigSvnUse;
in
callPackage ./generic.nix ( ( stdenv.lib.filterAttrs ( n: v: n != "callPackage" && n != "doubangoConfigSvn" && n != "fetchsvn" ) args ) // {
  doubangoConfig = (
    if doubangoConfigSvn.type == "trunk" then
      {
        _debug = builtins.trace "doubango/git.nix: ${stdenv.lib.showVal args}" null;
        src = fetchsvn {
          url = doubangoConfigSvn.url + "/trunk" + doubangoConfigSvn.urlSuffix;
          rev = doubangoConfigSvn.revision;
          sha256 = doubangoConfigSvn.sha256;
        };
        suffix = "-9999.0.0-r${doubangoConfigSvn.revision}-${doubangoConfigSvn.name}-svn-${doubangoConfigSvn.type}";
      } // doubangoConfigSvn
    else if doubangoConfigSvn.type == "branch" then
      {
        _debug = builtins.trace "doubango/git.nix: ${stdenv.lib.showVal args}" null;
        src = fetchsvn {
          url = doubangoConfigSvn.url + "/branches/" + doubangoConfigSvn.version + doubangoConfigSvn.urlSuffix;
          rev = doubangoConfigSvn.revision;
          sha256 = doubangoConfigSvn.sha256;
        };
        suffix = "-${doubangoConfigSvn.version}.9999-r${doubangoConfigSvn.revision}-${doubangoConfigSvn.name}-svn-${doubangoConfigSvn.type}-${doubangoConfigSvn.version}";
      } // doubangoConfigSvn
    else if doubangoConfigSvn.type == "tag" then
      {
        _debug = builtins.trace "doubango/git.nix: ${stdenv.lib.showVal args}" null;
        src = fetchsvn {
          url = doubangoConfigSvn.url + "/branches/" + doubangoConfigSvn.version + doubangoConfigSvn.urlSuffix;
          rev = doubangoConfigSvn.revision;
          sha256 = doubangoConfigSvn.sha256;
        };
        suffix = "-${doubangoConfigSvn.version}.9999-r${doubangoConfigSvn.revision}-${doubangoConfigSvn.name}-svn-${doubangoConfigSvn.type}-${doubangoConfigSvn.version}";
      } // doubangoConfigSvn
    else throw "Invalid doubangoConfigSvn.type ${doubangoConfigSvn.type} - need trunk/branch/tag"
  ); } )
