{ stdenv, srtp }:
stdenv.lib.overrideDerivation srtp (attrs: rec {
  name = "libsrtp-dl-${srtp.version}";
  patches = ( srtp.patches or [] ) ++ [ ./libsrtp-pcap-automagic-r0.patch ./libsrtp-1.5.2-fix-make-install.patch ./libsrtp-1.5.2-bindir.patch ];
  preBuild = ( srtp.preBuild or "" ) + ''
    buildFlagsArray+=( "all" "shared_library" )
  '';
  postInstall = "";
})
