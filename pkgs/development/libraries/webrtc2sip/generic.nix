{ stdenv, fetchsvn, pkgconfig, autoconf, automake, libtool
, doubango
, libxml2
, webrtc2sipConfig } @ args:

with stdenv.lib;

stdenv.mkDerivation rec {

  _debug_000 = builtins.trace "doubango: ${showVal doubango}" null;

  name = "webrtc2sip${webrtc2sipConfig.suffix}";

  src = webrtc2sipConfig.src;
  patches = webrtc2sipConfig.patches or [];
  sourceRoot = webrtc2sipConfig.sourceRoot or "";
  setSourceRoot = webrtc2sipConfig.setSourceRoot or "";
  sourceRootSuffix = webrtc2sipConfig.sourceRootSuffix or "";
  postUnpack = "sourceRoot=\${sourceRoot}${sourceRootSuffix}";

  enableParallelBuilding = true;

  buildInputs = [ pkgconfig autoconf automake libtool doubango libxml2 ];

  preConfigure = ''
    ./autogen.sh
  '';

  configureFlags = ''
    --with-libxml2=${libxml2}
  '';

  meta = {
    description = "Gateway using RTCWeb and SIP to turn your browser into a phone with audio, video and SMS capabilities.";
    longDescription = ''
        webrtc2sip is a smart and powerful gateway using RTCWeb and SIP to turn your browser into a phone with audio, video and SMS capabilities.
    '';
    homepage = https://code.google.com/p/webrtc2sip/;
    license = stdenv.lib.licenses.gpl3Plus;
    maintainers = [ "Iwan Aucamp <aucampia@gmail.com>" ];
    platforms = stdenv.lib.platforms.all;
  };
}
