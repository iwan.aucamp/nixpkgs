{ callPackage, webrtc2sipConfigGit, fetchgit, stdenv, ... } @ args:

let
  webrtc2sipConfigGitUse = { url = "https://gitlab.com/doubango-ia/webrtc2sip.git"; } // webrtc2sipConfigGit;
in
let
  webrtc2sipConfigGit = webrtc2sipConfigGitUse;
in
callPackage ./generic.nix ( ( stdenv.lib.filterAttrs ( n: v: n != "callPackage" && n != "webrtc2sipConfigGit" && n != "fetchgit" ) args ) // {
  webrtc2sipConfig =
    {
      src = fetchgit {
        url = webrtc2sipConfigGit.url;
        rev = webrtc2sipConfigGit.revision;
        sha256 = webrtc2sipConfigGit.sha256;
      };
      suffix = "-${webrtc2sipConfigGit.version}-h${builtins.substring 0 7 webrtc2sipConfigGit.revision}-${webrtc2sipConfigGit.name}-git-${webrtc2sipConfigGit.type}";
    } // webrtc2sipConfigGit;
} )
