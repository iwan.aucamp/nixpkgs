{ callPackage, webrtc2sipConfigSvn, fetchsvn, stdenv, ... } @ args:

let
  webrtc2sipConfigSvnUse = { url = "https://webrtc2sip.googlecode.com/svn"; urlSuffix = ""; } // webrtc2sipConfigSvn;
in
let
  webrtc2sipConfigSvn = webrtc2sipConfigSvnUse;
in
callPackage ./generic.nix ( ( stdenv.lib.filterAttrs ( n: v: n != "callPackage" && n != "webrtc2sipConfigSvn" && n != "fetchsvn" ) args ) // {
  webrtc2sipConfig = (
    if webrtc2sipConfigSvn.type == "trunk" then
      {
        src = fetchsvn {
          url = webrtc2sipConfigSvn.url + "/trunk" + webrtc2sipConfigSvn.urlSuffix;
          rev = webrtc2sipConfigSvn.revision;
          sha256 = webrtc2sipConfigSvn.sha256;
        };
        suffix = "-9999.0.0-r${webrtc2sipConfigSvn.revision}-${webrtc2sipConfigSvn.name}-svn-${webrtc2sipConfigSvn.type}";
      } // webrtc2sipConfigSvn
    else if webrtc2sipConfigSvn.type == "branch" then
      {
        src = fetchsvn {
          url = webrtc2sipConfigSvn.url + "/branches/" + webrtc2sipConfigSvn.version + webrtc2sipConfigSvn.urlSuffix;
          rev = webrtc2sipConfigSvn.revision;
          sha256 = webrtc2sipConfigSvn.sha256;
        };
        suffix = "-${webrtc2sipConfigSvn.version}.9999-r${webrtc2sipConfigSvn.revision}-${webrtc2sipConfigSvn.name}-svn-${webrtc2sipConfigSvn.type}-${webrtc2sipConfigSvn.version}";
      } // webrtc2sipConfigSvn
    else if webrtc2sipConfigSvn.type == "tag" then
      {
        src = fetchsvn {
          url = webrtc2sipConfigSvn.url + "/branches/" + webrtc2sipConfigSvn.version + webrtc2sipConfigSvn.urlSuffix;
          rev = webrtc2sipConfigSvn.revision;
          sha256 = webrtc2sipConfigSvn.sha256;
        };
        suffix = "-${webrtc2sipConfigSvn.version}.9999-r${webrtc2sipConfigSvn.revision}-${webrtc2sipConfigSvn.name}-svn-${webrtc2sipConfigSvn.type}-${webrtc2sipConfigSvn.version}";
      } // webrtc2sipConfigSvn
    else throw "Invalid webrtc2sipConfigSvn.type ${webrtc2sipConfigSvn.type} - need trunk/branch/tag"
  ); } )
